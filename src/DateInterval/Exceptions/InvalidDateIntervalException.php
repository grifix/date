<?php

declare(strict_types=1);

namespace Grifix\Date\DateInterval\Exceptions;

final class InvalidDateIntervalException extends \Exception
{

    public function __construct(\Throwable $previous)
    {
        parent::__construct('Invalid date interval! ', 0, $previous);
    }
}
