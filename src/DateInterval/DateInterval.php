<?php

declare(strict_types=1);

namespace Grifix\Date\DateInterval;

use DateInterval as PhpDateInterval;
use Grifix\BigInt\BigInt;
use Grifix\Date\DateInterval\Exceptions\InvalidDateIntervalException;
use Grifix\Date\DateTime\DateTime;

final class DateInterval
{
    private PhpDateInterval $interval;

    public function __construct(
        \DateTimeImmutable $initialDate,
        PhpDateInterval $interval
    ) {
        $toDate = $initialDate->add($interval);
        $this->interval = $initialDate->diff($toDate, true);
    }


    public static function create(
        DateTime $initialDate = null,
        ?int $years = null,
        ?int $months = null,
        ?int $weeks = null,
        ?int $days = null,
        ?int $hours = null,
        ?int $minutes = null,
        ?int $seconds = null

    ): self {
        if (null === $initialDate) {
            $initialDate = new DateTime(new \DateTimeImmutable());
        }

        $period = 'P';
        if (null !== $years) {
            $period .= $years . 'Y';
        }
        if (null !== $months) {
            $period .= $months . 'M';
        }
        if (null !== $weeks) {
            $period .= $weeks . 'W';
        }
        if (null !== $days) {
            $period .= $days . 'D';
        }
        if ($minutes || $hours || $seconds) {
            $period .= 'T';
        }
        if (null !== $hours) {
            $period .= $hours . 'H';
        }
        if (null !== $minutes) {
            $period .= $minutes . 'M';
        }
        if (null !== $seconds) {
            $period .= $seconds . 'S';
        }
        try {
            $interval = new PhpDateInterval($period);
        } catch (\Throwable $exception) {
            throw new InvalidDateIntervalException($exception);
        }

        return new self($initialDate->getWrapped(), $interval);
    }

    public function getYears(): int
    {
        return $this->interval->y;
    }

    public function getMonths(): int
    {
        return $this->interval->m;
    }

    public function getDays(): int
    {
        return $this->interval->d;
    }

    public function getHours(): int
    {
        return $this->interval->h;
    }

    public function getMinutes(): int
    {
        return $this->interval->i;
    }

    public function getSeconds(): int
    {
        return $this->interval->s;
    }

    public function getMicroseconds(): float
    {
        return $this->interval->f;
    }

    public function format(string $format): string
    {
        return $this->interval->format($format);
    }

    public function getWrapped(): PhpDateInterval
    {
        return $this->interval;
    }

    public function getFullDays(): BigInt
    {
        return BigInt::create($this->interval->days);
    }

    public function getFullHours(): BigInt
    {
        return BigInt::create($this->interval->days)->multiple(24)->add($this->getHours());
    }

    public function getFullMinutes(): BigInt
    {
        return $this->getFullHours()->multiple(60)->add($this->getMinutes());
    }

    public function getFullSeconds(): BigInt
    {
        return $this->getFullMinutes()->multiple(60)->add($this->getSeconds());
    }

    public function toString(): string
    {
        return $this->format('%y years %m months %d days %h hours %i minutes %s seconds %f microseconds');
    }

    public function isEqualTo(DateInterval $other): bool
    {
        if ($this->interval->days !== $other->interval->days) {
            return false;
        }

        if ($this->getHours() !== $other->getHours()) {
            return false;
        }

        if ($this->getMinutes() !== $other->getMinutes()) {
            return false;
        }

        if ($this->getSeconds() !== $other->getSeconds()) {
            return false;
        }

        if ($this->getMicroseconds() !== $other->getMicroseconds()) {
            return false;
        }

        return true;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
