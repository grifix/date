<?php

declare(strict_types=1);

namespace Grifix\Date\TimeZone;

final class Location
{
    public function __construct(
        public readonly string $countryCode,
        public readonly float $latitude,
        public readonly float $longitude,
        public readonly string $comments
    ) {
    }

    public function toString(): string
    {
        return sprintf(
            '%s (%F, %F)',
            $this->countryCode,
            $this->latitude,
            $this->longitude
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
