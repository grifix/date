<?php

declare(strict_types=1);

namespace Grifix\Date\TimeZone;

use DateTimeZone as PhpDateTimeZone;
use Grifix\Date\TimeZone\Exceptions\InvalidTimeZoneException;

final class TimeZone
{

    public function __construct(private readonly PhpDateTimeZone $timeZone)
    {
    }

    public function getWrapped(): PhpDateTimeZone
    {
        return $this->timeZone;
    }

    public static function utc(): self
    {
        return new self(new PhpDateTimeZone('UTC'));
    }

    public static function cet(): self
    {
        return new self(new PhpDateTimeZone('CET'));
    }

    public static function fromString(string $value): self
    {
        try {
            return new self(new PhpDateTimeZone($value));
        } catch (\Throwable $exception) {
            throw new InvalidTimeZoneException($exception);
        }
    }

    public function getName(): string
    {
        return $this->timeZone->getName();
    }

    public function getLocation(): ?Location
    {
        $location = $this->timeZone->getLocation();
        if (!$location) {
            return null;
        }
        return new Location(
            $location['country_code'],
            (float)$location['latitude'],
            (float)$location['longitude'],
            $location['comments']
        );
    }
}
