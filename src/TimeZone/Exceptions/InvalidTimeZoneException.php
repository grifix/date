<?php

declare(strict_types=1);

namespace Grifix\Date\TimeZone\Exceptions;

final class InvalidTimeZoneException extends \Exception
{

    public function __construct(\Throwable $previous)
    {
        parent::__construct('Invalid timezone!', 0, $previous);
    }
}
