<?php

declare(strict_types=1);

namespace Grifix\Date\DateTime\Exceptions;

final class InvalidDateFormatException extends \Exception
{

    public function __construct(string $date, string $format)
    {
        parent::__construct(sprintf('The date [%s] does not match the format [%s]!', $date, $format));
    }
}
