<?php

declare(strict_types=1);

namespace Grifix\Date\DateTime\Exceptions;

final class InvalidDateTimeException extends \Exception
{

    public function __construct(?\Throwable $previous)
    {
        parent::__construct('Invalid date-time!', 0, $previous);
    }
}
