<?php

declare(strict_types=1);

namespace Grifix\Date\DateTime;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\Exceptions\InvalidDateFormatException;
use Grifix\Date\DateTime\Exceptions\InvalidDateTimeException;
use Grifix\Date\TimeZone\TimeZone;

final class DateTime
{
    private const PG_TIMESTAMP_FORMAT = 'Y-m-d H:i:s.u';

    public function __construct(private readonly \DateTimeImmutable $value)
    {
    }

    public static function fromAtom(string $value): self
    {
        $date = \DateTimeImmutable::createFromFormat(\DateTimeInterface::ATOM, $value);
        if (false === $date) {
            throw new InvalidDateFormatException($value, \DateTimeInterface::ATOM);
        }
        return new self($date);
    }

    public static function fromString(string $value): self
    {
        return new self(new \DateTimeImmutable($value));
    }

    public static function fromPgTimestamp(string $value): self
    {
        $originalValue = $value;
        if (!str_contains($value, '.')) {
            $value = $value . '.000000';
        }
        $date = \DateTimeImmutable::createFromFormat(self::PG_TIMESTAMP_FORMAT, $value);
        if (false === $date) {
            throw new InvalidDateFormatException($originalValue, self::PG_TIMESTAMP_FORMAT);
        }

        return new self($date);
    }

    public static function fromTimestamp(int $value): self
    {
        return new self(\DateTimeImmutable::createFromFormat('U', (string)$value));
    }

    public static function create(
        int $year,
        int $month,
        int $day,
        int $hour = 0,
        int $minute = 0,
        int $second = 0,
        int $microsecond = 0,
        ?TimeZone $timeZone = null
    ): self {
        if (null === $timeZone) {
            $timeZone = TimeZone::utc();
        }
        if ($minute < 10) {
            $minute = '0' . $minute;
        }
        if ($second < 10) {
            $second = '0' . $second;
        }

        $date = \DateTimeImmutable::createFromFormat(
            'Y-n-j G:i:s u',
            sprintf(
                '%s-%s-%s %s:%s:%s %s',
                $year,
                $month,
                $day,
                $hour,
                $minute,
                $second,
                self::formatMicrosecond($microsecond)
            ),
            $timeZone->getWrapped()
        );
        if (false === $date) {
            throw new InvalidDateTimeException(null);
        }

        try {
            return new self($date);
        } catch (\Throwable $exception) {
            throw new InvalidDateTimeException($exception);
        }
    }

    private static function formatMicrosecond(int $microsecond): string
    {
        $result = (string)$microsecond;
        $number = 6 - strlen($result);
        for ($i = 0; $i < $number; $i++) {
            $result = '0' . $result;
        }
        return $result;
    }

    public function toAtom(): string
    {
        return $this->value->format(\DateTimeInterface::ATOM);
    }

    public function format(string $format): string
    {
        return $this->value->format($format);
    }

    public function add(DateInterval $interval): self
    {
        return new self($this->getWrapped()->add($interval->getWrapped()));
    }

    public function subtract(DateInterval $interval): self
    {
        return new self($this->getWrapped()->sub($interval->getWrapped()));
    }

    public function isEqualTo(DateTime $other): bool
    {
        return $this->value == $other->value;
    }

    public function isBefore(DateTime $other): bool
    {
        return $this->value < $other->value;
    }

    public function isAfter(DateTime $other): bool
    {
        return $this->value > $other->value;
    }

    public function getWrapped(): \DateTimeImmutable
    {
        return $this->value;
    }

    public function diff(DateTime $other): DateInterval
    {
        return new DateInterval($this->getWrapped(), $this->value->diff($other->value, true));
    }

    public function getTimezone(): TimeZone
    {
        return new TimeZone($this->value->getTimezone());
    }

    public function getYear(): int
    {
        return (int)$this->value->format('Y');
    }

    public function getMonth(): int
    {
        return (int)$this->value->format('n');
    }

    public function getMonthDay(): int
    {
        return (int)$this->value->format('j');
    }

    public function getWeekDay(): int
    {
        return (int)$this->value->format('w');
    }

    public function getYearDay(): int
    {
        return (int)$this->value->format('z');
    }

    public function getHour(): int
    {
        return (int)$this->value->format('G');
    }

    public function getMinute(): int
    {
        return (int)$this->value->format('i');
    }

    public function getSecond(): int
    {
        return (int)$this->value->format('s');
    }

    public function getMicrosecond(): int
    {
        return (int)$this->value->format('u');
    }

    public function isSunday(): bool
    {
        return $this->getWeekDay() === 0;
    }

    public function isMonday(): bool
    {
        return $this->getWeekDay() === 1;
    }

    public function isTuesday(): bool
    {
        return $this->getWeekDay() === 2;
    }

    public function isWednesday(): bool
    {
        return $this->getWeekDay() === 3;
    }

    public function isThursday(): bool
    {
        return $this->getWeekDay() === 4;
    }

    public function isFriday(): bool
    {
        return $this->getWeekDay() === 5;
    }

    public function isSaturday(): bool
    {
        return $this->getWeekDay() === 6;
    }

    public function isLastDayInMonth(): bool
    {
        return $this->getMonthDay() === (int)$this->format('t');
    }

    public function isYearLeap(): bool
    {
        return '1' === $this->format('L');
    }

    public function getOffset(): int
    {
        return $this->value->getOffset();
    }

    public function getTimestamp(): int
    {
        return $this->value->getTimestamp();
    }

    public function toPgTimestamp(): string
    {
        return $this->value->format(self::PG_TIMESTAMP_FORMAT);
    }

    public function __toString(): string
    {
        return $this->toAtom();
    }
}
