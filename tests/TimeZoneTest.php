<?php

declare(strict_types=1);

namespace Grifix\Date\Tests;

use Grifix\Date\TimeZone\Exceptions\InvalidTimeZoneException;
use Grifix\Date\TimeZone\Location;
use Grifix\Date\TimeZone\TimeZone;
use PHPUnit\Framework\TestCase;

final class TimeZoneTest extends TestCase
{
    public function testItCreates(): void
    {
        $timeZone = TimeZone::cet();
        self::assertEquals('CET', $timeZone->getName());
        self:
        self::assertEquals($timeZone->getWrapped(), new \DateTimeZone('CET'));

        $timeZone = TimeZone::utc();
        self::assertEquals('UTC', $timeZone->getName());
    }

    public function testItGetsLocation(): void
    {
        $timeZone = TimeZone::fromString('Europe/Warsaw');
        self::assertEquals(
            new Location(
                'PL',
                52.25,
                21.0,
                ''
            ),
            $timeZone->getLocation()
        );
        self::assertEquals('PL (52.250000, 21.000000)', $timeZone->getLocation()->toString());
        self::assertEquals('PL (52.250000, 21.000000)', (string)$timeZone->getLocation());

        $timeZone = TimeZone::cet();
        self::assertNull($timeZone->getLocation());
    }

    public function testItDoesNotCreate(): void
    {
        $this->expectException(InvalidTimeZoneException::class);
        $this->expectExceptionMessage('Invalid timezone!');
        TimeZone::fromString('invalid_timezone');
    }
}
