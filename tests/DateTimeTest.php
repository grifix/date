<?php

declare(strict_types=1);

namespace Grifix\Date\Tests;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Date\DateTime\Exceptions\InvalidDateFormatException;
use Grifix\Date\TimeZone\TimeZone;
use PHPUnit\Framework\TestCase;

final class DateTimeTest extends TestCase
{
    public function testItCreatesFromAtom(): void
    {
        $dateTime = DateTime::fromAtom('2009-11-04T19:55:41Z');
        self::assertEquals('2009-11-04T19:55:41+00:00', $dateTime->toAtom());
        self::assertEquals('2009-11-04T19:55:41+00:00', (string)$dateTime);
    }

    public function testItCreatesFromPgTimeStamp(): void
    {
        $dateTime = DateTime::fromPgTimeStamp('2001-01-01 12:00:00.125812');
        self::assertEquals('2001-01-01 12:00:00.125812', $dateTime->toPgTimestamp());

        $dateTime = DateTime::fromPgTimeStamp('2001-01-01 12:00:00');
        self::assertEquals('2001-01-01 12:00:00.000000', $dateTime->toPgTimestamp());
    }

    public function testItCreatesFromTimeStamp(): void
    {
        $timeStamp = 1673594194;
        self::assertEquals($timeStamp, DateTime::fromTimestamp($timeStamp)->getTimestamp());
    }

    public function testItFailsToCreateFromWrongPgTime(): void
    {
        $this->expectException(InvalidDateFormatException::class);
        $this->expectExceptionMessage('The date [12:00] does not match the format [Y-m-d H:i:s.u]!');
        DateTime::fromPgTimestamp('12:00');
    }

    public function testItFailsToCreateFromWrongAtom(): void
    {
        $this->expectException(InvalidDateFormatException::class);
        $this->expectExceptionMessage('The date [12:00] does not match the format [Y-m-d\TH:i:sP]!');
        DateTime::fromAtom('12:00');
    }

    public function testItCreates(): void
    {
        $dateTime = DateTime::create(
            2020,
            2,
            12,
            18,
            33,
            12,
            10,
            TimeZone::cet()
        );
        self::assertEquals('2020-02-12T18:33:12+01:00', $dateTime->toAtom());
        self::assertEquals(2020, $dateTime->getYear());
        self::assertEquals(2, $dateTime->getMonth());
        self::assertEquals(12, $dateTime->getMonthDay());
        self::assertEquals(18, $dateTime->getHour());
        self::assertEquals(33, $dateTime->getMinute());
        self::assertEquals(12, $dateTime->getSecond());
        self::assertEquals(10, $dateTime->getMicrosecond());
        self::assertEquals(TimeZone::cet(), $dateTime->getTimezone());
        self::assertEquals(1581528792, $dateTime->getTimestamp());
        self::assertFalse($dateTime->isSunday());
        self::assertFalse($dateTime->isMonday());
        self::assertFalse($dateTime->isTuesday());
        self::assertTrue($dateTime->isWednesday());
        self::assertFalse($dateTime->isThursday());
        self::assertFalse($dateTime->isFriday());
        self::assertFalse($dateTime->isSaturday());
        self::assertEquals(42, $dateTime->getYearDay());
        self::assertFalse($dateTime->isLastDayInMonth());
        self::assertEquals(3600, $dateTime->getOffset());
        self::assertTrue($dateTime->isYearLeap());


        $dateTime = DateTime::create(
            2020,
            2,
            12,
            18,
            33,
            12
        );
        self::assertEquals(TimeZone::utc(), $dateTime->getTimezone());
    }

    public function testItFormats(): void
    {
        $dateTime = DateTime::create(2020, 01, 01);
        self::assertEquals('2020-01-01', $dateTime->format('Y-m-d'));
    }

    public function testItAdds(): void
    {
        $date = DateTime::create(2000, 1, 1);
        $newDate = $date->add(DateInterval::create(months: 1));
        self::assertEquals($newDate, DateTime::create(2000, 2, 1));
    }

    public function testItSubtracts(): void
    {
        $date = DateTime::create(2000, 2, 1);
        $newDate = $date->subtract(DateInterval::create(months: 1));
        self::assertEquals($newDate, DateTime::create(2000, 1, 1));
    }

    public function testIsEqual(): void
    {
        $date = DateTime::create(2000, 1, 1);
        self::assertFalse($date->isEqualTo(DateTime::create(2000, 2, 1)));
        self::assertTrue($date->isEqualTo(DateTime::create(2000, 1, 1)));
    }

    public function testIsBefore(): void
    {
        $date = DateTime::create(2000, 1, 1);
        self::assertTrue($date->isBefore(DateTime::create(2000, 2, 1)));
        self::assertFalse($date->isBefore(DateTime::create(1999, 1, 1)));
    }

    public function testIsAfter(): void
    {
        $date = DateTime::create(2000, 1, 1);
        self::assertTrue($date->isAfter(DateTime::create(1999, 2, 1)));
        self::assertFalse($date->isAfter(DateTime::create(2001, 1, 1)));
    }

    public function testItDiffs(): void
    {
        $date = DateTime::create(2000, 1, 1);
        self::assertTrue(
            DateInterval::create(days: 60)->isEqualTo(
                $date->diff(DateTime::create(2000, 3, 1))
            ),
        );
    }
}
