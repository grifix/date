<?php

declare(strict_types=1);

namespace Grifix\Date\Tests\Builders;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;

final class DateIntervalBuilder
{
    private ?int $years = null;

    private ?int $months = null;

    private ?int $weeks = null;

    private ?int $days = null;

    private ?int $hours = null;

    private ?int $minutes = null;

    private ?int $seconds = null;

    private ?DateTime $fromDate = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getYears(): ?int
    {
        return $this->years;
    }

    public function getMonths(): ?int
    {
        return $this->months;
    }

    public function getWeeks(): ?int
    {
        return $this->weeks;
    }

    public function getDays(): ?int
    {
        return $this->days;
    }

    public function getHours(): ?int
    {
        return $this->hours;
    }

    public function getMinutes(): ?int
    {
        return $this->minutes;
    }

    public function getSeconds(): ?int
    {
        return $this->seconds;
    }


    public function setYears(?int $years): self
    {
        $this->years = $years;

        return $this;
    }

    public function setMonths(?int $months): self
    {
        $this->months = $months;

        return $this;
    }

    public function setWeeks(?int $weeks): self
    {
        $this->weeks = $weeks;

        return $this;
    }

    public function setDays(?int $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function setHours(?int $hours): self
    {
        $this->hours = $hours;

        return $this;
    }

    public function setMinutes(?int $minutes): self
    {
        $this->minutes = $minutes;

        return $this;
    }

    public function setSeconds(?int $seconds): self
    {
        $this->seconds = $seconds;

        return $this;
    }

    public function getFromDate(): ?DateTime
    {
        return $this->fromDate;
    }

    public function setFromDate(?DateTime $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function build(): DateInterval
    {
        return DateInterval::create(
            $this->fromDate,
            $this->years,
            $this->months,
            $this->weeks,
            $this->days,
            $this->hours,
            $this->minutes,
            $this->seconds
        );
    }
}
