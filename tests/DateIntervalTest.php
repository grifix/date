<?php

declare(strict_types=1);

namespace Grifix\Date\Tests;

use Grifix\BigInt\BigInt;
use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Date\Tests\Builders\DateIntervalBuilder;
use PHPUnit\Framework\TestCase;

final class DateIntervalTest extends TestCase
{
    /**
     * @dataProvider itCreatesDataProvider
     */
    public function testItCreates(
        DateIntervalBuilder $builder,
        string $expectedToStringResult,
        BigInt $expectedFullDays,
        BigInt $expectedFullHours,
        BigInt $expectedFullMinutes,
        BigInt $expectedFullSeconds,
    ): void {
        $interval = $builder->build();

        self::assertEquals(
            $expectedToStringResult,
            $interval->toString()
        );

        self::assertEquals(
            $expectedToStringResult,
            (string)$interval
        );

        self::assertEquals($expectedFullDays, $interval->getFullDays());
        self::assertEquals($expectedFullHours, $interval->getFullHours());
        self::assertEquals($expectedFullMinutes, $interval->getFullMinutes());
        self::assertEquals($expectedFullSeconds, $interval->getFullSeconds());
    }

    public function itCreatesDataProvider(): array
    {
        return [
            '5 years' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setYears(5),
                '5 years 0 months 0 days 0 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(1827),
                BigInt::create(43848),
                BigInt::create(2630880),
                BigInt::create(157852800)
            ],
            '20 months' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setMonths(20),
                '1 years 8 months 0 days 0 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(609),
                BigInt::create(14616),
                BigInt::create(876960),
                BigInt::create(52617600)
            ],

            '35 days' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setDays(35),
                '0 years 1 months 4 days 0 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(35),
                BigInt::create(840),
                BigInt::create(50400),
                BigInt::create(3024000)
            ],
            '28 hours' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setHours(28),
                '0 years 0 months 1 days 4 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(1),
                BigInt::create(28),
                BigInt::create(1680),
                BigInt::create(100800)
            ],

            '90 minutes' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setMinutes(90),
                '0 years 0 months 0 days 1 hours 30 minutes 0 seconds 0 microseconds',
                BigInt::create(0),
                BigInt::create(1),
                BigInt::create(90),
                BigInt::create(5400)
            ],

            '90 seconds' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setSeconds(90),
                '0 years 0 months 0 days 0 hours 1 minutes 30 seconds 0 microseconds',
                BigInt::create(0),
                BigInt::create(0),
                BigInt::create(1),
                BigInt::create(90)
            ],

            '1 year and 30 seconds' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setYears(1)
                    ->setSeconds(30)
                ,
                '1 years 0 months 0 days 0 hours 0 minutes 30 seconds 0 microseconds',
                BigInt::create(366),
                BigInt::create(8784),
                BigInt::create(527040),
                BigInt::create(31622430)
            ],

            'without date' => [
                DateIntervalBuilder::create()->setDays(1),
                '0 years 0 months 1 days 0 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(1),
                BigInt::create(24),
                BigInt::create(1440),
                BigInt::create(86400)
            ],

            '1 week' => [
                DateIntervalBuilder::create()->setWeeks(1),
                '0 years 0 months 7 days 0 hours 0 minutes 0 seconds 0 microseconds',
                BigInt::create(7),
                BigInt::create(168),
                BigInt::create(10080),
                BigInt::create(604800)
            ]
        ];
    }

    public function testGetters(): void
    {
        $interval = DateIntervalBuilder::create()
            ->setYears(1)
            ->setMonths(2)
            ->setDays(3)
            ->setHours(4)
            ->setMinutes(5)
            ->setSeconds(6)
            ->build();

        self::assertEquals(1, $interval->getYears());
        self::assertEquals(2, $interval->getMonths());
        self::assertEquals(3, $interval->getDays());
        self::assertEquals(4, $interval->getHours());
        self::assertEquals(5, $interval->getMinutes());
        self::assertEquals(6, $interval->getSeconds());
        self::assertInstanceOf(\DateInterval::class, $interval->getWrapped());
    }

    /**
     * @dataProvider isEqualToDataProvider
     */
    public function testIsEqualTo(DateInterval $intervalA, DateInterval $intervalB, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, $intervalA->isEqualTo($intervalB));
    }

    public function isEqualToDataProvider(): array
    {
        return [
            'same days' => [
                DateIntervalBuilder::create()->setDays(5)->build(),
                DateIntervalBuilder::create()->setDays(5)->build(),
                true
            ],
            'not same days' => [
                DateIntervalBuilder::create()->setDays(6)->build(),
                DateIntervalBuilder::create()->setDays(5)->build(),
                false
            ],
            'leap year' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2000, 1, 1))
                    ->setMonths(12)
                    ->build(),
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2001, 1, 1))
                    ->setMonths(12)
                    ->build(),
                false
            ],
            'not leap year' => [
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2001, 1, 1))
                    ->setMonths(12)
                    ->build(),
                DateIntervalBuilder::create()
                    ->setFromDate(DateTime::create(2002, 1, 1))
                    ->setMonths(12)
                    ->build(),
                true
            ],
            'different hours' => [
                DateIntervalBuilder::create()
                    ->setHours(2)
                    ->build(),
                DateIntervalBuilder::create()
                    ->setHours(4)
                    ->build(),
                false
            ],
            'different minutes' => [
                DateIntervalBuilder::create()
                    ->setMinutes(2)
                    ->build(),
                DateIntervalBuilder::create()
                    ->setMinutes(4)
                    ->build(),
                false
            ],
            'different seconds' => [
                DateIntervalBuilder::create()
                    ->setSeconds(2)
                    ->build(),
                DateIntervalBuilder::create()
                    ->setSeconds(4)
                    ->build(),
                false
            ],
            'different microseconds' => [
                DateTime::create(year: 2000, month: 1, day: 1, microsecond: 100)
                    ->diff(DateTime::create(year: 2000, month: 1, day: 1, microsecond: 50)),
                DateTime::create(year: 2000, month: 1, day: 1, microsecond: 100)
                    ->diff(DateTime::create(year: 2000, month: 1, day: 1, microsecond: 60)),
                false
            ]
        ];
    }
}
